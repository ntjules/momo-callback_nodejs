const express = require('express');
const bodyParser = require('body-parser');

const app = express();
// app.use(require('body-parser').json({type : '*/*'}));
app.use (function(req, res, next) {
    var data='';
    req.setEncoding('utf8');
    req.on('data', function(chunk) { 
       data += chunk;
    });

    req.on('end', function() {
        req.body = data;
        next();
    });
});

app.post('/callback', (req, res) => {

var reqst = JSON.parse(req.body); 

console.log(reqst)

console.log("\n---------------------------------------")
console.log("status: ",reqst.state)
console.log("message: ",reqst.message)
console.log("---------------------------------------")


    res.sendStatus(200);
});

app.listen(process.env.PORT || 8080, () => console.log(`Started server `));